
#ifndef SNRRpulseDelay
    #define SNRRpulseDelay 8
#endif

#ifndef SNRRpulseWidth  
    #define SNRRpulseWidth 4
#endif

#ifndef SNRRport   
    #define SNRRport PIN_A2
#endif

#ifndef SNRRenable   
    #define SNRRenable PIN_A3
#endif

unsigned int8 SNRR_SendData[6];

void SNRR_ClearBuffer();
void SNRR_Send(unsigned int8 addr, unsigned int8 param);
void SNRR_SendPreambule();
void SNRR_SendPulse();
void SNRR_SendZero();
void SNRR_SendOne();
void SNRR_SendByte(unsigned int8 data, unsigned int8 *crc);

//=====================================================================================================
void SNRR_ClearBuffer() {
    for (unsigned int8 i = 0; i < 6; i ++) SNRR_SendData[i] = 0;
}

void SNRR_Send(unsigned int8 addr, unsigned int8 param) {
    output_high(SNRRenable);  
    delay_us(10);
    output_high(SNRRport); 
    delay_us(500);
    unsigned int8 a = (addr << 4) + param;
    unsigned int8 crc = 0;
    SNRR_SendPreambule();
    SNRR_SendByte(a, &crc);
    for (unsigned int8 i = 0; i < 6; i ++) SNRR_SendByte(SNRR_SendData[i], &crc);
    SNRR_SendByte(crc, &crc);
    SNRR_SendPulse();
    delay_us(500);
    output_low(SNRRport);      
    delay_us(10);
    output_low(SNRRenable); 
}

void SNRR_SendPreambule() {
    output_low(SNRRport);
    delay_us(2226);
     output_high(SNRRport);
    delay_us(3100);//3300
}

void SNRR_SendPulse() {
    output_low(SNRRport);
    delay_us(300);
    output_high(SNRRport);
}

void SNRR_SendZero() {
    SNRR_SendPulse();
    delay_us(445);
}

void SNRR_SendOne() {
    SNRR_SendPulse();
    delay_us(1312);
}

void SNRR_SendByte(unsigned int8 data, unsigned int8 *crc) {
    unsigned int8 d = data;
    *crc = *crc ^ data;
    for (int b = 0; b < 8; b ++) {
        if (d & 0b10000000) SNRR_SendOne();
        else SNRR_SendZero();
        d = d << 1;
    }
}