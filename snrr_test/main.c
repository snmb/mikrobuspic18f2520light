#include <main.h>
#include "snrr.c"



void send1() {
    output_high(LED1);
    SNRR_ClearBuffer();
    SNRR_SendData[0] = 5;
    SNRR_SendData[1] = 16;
    SNRR_Send(1, 1);
    output_low(LED1); 
}

void send2() {
    output_high(LED2);
    SNRR_ClearBuffer();
    SNRR_SendData[0] = 7;
    SNRR_SendData[1] = 0;
    SNRR_SendData[2] = 16;
    SNRR_Send(1, 2);  
    output_low(LED2);  
}

void main() {
    SET_TRIS_A( 0x31 );
    SET_TRIS_B( 0x01 );
    SET_TRIS_C( 0x00 );

    output_low(TX); //SendData
    output_low(CS); //Enable
    output_low(LED3); 
    
    while(TRUE) {
      if (!input_state(BUTTON1)) {
         send1();
         delay_ms(500); 
      }

      if (!input_state(BUTTON2)) {
         send2();
         delay_ms(500); 
      }
   }

}
