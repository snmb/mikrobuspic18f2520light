#include <18F2520.h>

#use delay(crystal=20000000)

#define LED1  PIN_A1
#define LED2  PIN_A2
#define LED3  PIN_A3

#define BUTTON1  PIN_A4
#define BUTTON2  PIN_A5

#define AN     PIN_A0
#define RST    PIN_B3
#define CS     PIN_B1
#define SCK    PIN_C3
#define MISO   PIN_C5
#define MOSI   PIN_C4

#define PWM    PIN_C2
#define pINT   PIN_B0
#define RX     PIN_C7
#define TX     PIN_C6
#define SCL    PIN_C3
#define SDA    PIN_C4

#define DB1    PIN_B7
#define DB2    PIN_B6

#define SNRRport TX
#define SNRRenable CS
